/*Test Programm fr� GIT Versionierung */


data Meine_CARS_Daten;
	set sashelp.cars;
run;

%let var_list=Make Type Cylinders Horsepower Invoice Length;

proc print data=Meine_CARS_Daten noobs;
	var &var_list.;
run;
