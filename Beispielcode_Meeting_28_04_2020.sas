/* test Programm zur Versionierung */

data interesting_cars;
	set sashelp.cars;
	where type="Sports";
run;

%let print_vars=Make Model Origin Invoice Horsepower Weight Length;

proc print data=interesting_cars;
	var &print_vars.;
/* sum st. ? */
run;


/* proc means ? */
proc means data=interesting_cars;
run;


data _null_;
 call symput ("heute",put(today(),date9.));
 run;

 %put &heute.;



 /* Hier der Test der gitfn_push Funktion */
 /* --> erst ab SAS 9.4 Maintenance 6 */
 data _null_;
	 rc=gitfn_push("D:\Kybeidos\CoBa_GIT_Projekt\SAS_Programs\git_test_nr1_repo");
	 put rc=;
 run;